package com.experienceitsolutions.resttest.service;

import com.experienceitsolutions.resttest.model.IUser;

import java.util.List;

public interface IUserService<T extends IUser<ID>, ID> {

    T createUser();

    List<T> findAll();

}
