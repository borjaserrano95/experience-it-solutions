package com.experienceitsolutions.resttest.service;

import com.experienceitsolutions.resttest.model.User;
import com.experienceitsolutions.resttest.repository.UserRepository;
import com.experienceitsolutions.resttest.utils.UserValidation;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService implements IUserService<User, Long> {

    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User createUser() {
        long id = userRepository.count() + 1;
        User user = new User("name" + id, "username" + id, "password" + id);
        UserValidation.validateUserCreation(user);
        return userRepository.save(user);
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }
}
