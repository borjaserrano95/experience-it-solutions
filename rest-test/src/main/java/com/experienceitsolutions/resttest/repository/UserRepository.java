package com.experienceitsolutions.resttest.repository;

import com.experienceitsolutions.resttest.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
}
