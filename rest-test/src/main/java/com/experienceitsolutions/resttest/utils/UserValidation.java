package com.experienceitsolutions.resttest.utils;

import com.experienceitsolutions.resttest.model.IUser;

public class UserValidation {

    public static <T extends IUser<?>> void validateUserCreation(T user) {
        if (user == null) {
            throw new IllegalArgumentException("User cannot be null");
        }
        if (user.getId() != null) {
            throw new IllegalArgumentException("User id must be null");
        }
        if (user.getUsername() == null || user.getUsername().isEmpty()) {
            throw new IllegalArgumentException("Username cannot be null or empty");
        }
        if (user.getPassword() == null || user.getPassword().isEmpty()) {
            throw new IllegalArgumentException("Password cannot be null or empty");
        }
    }
}
