package com.experienceitsolutions.resttest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExperienceItSolutionsRestTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(ExperienceItSolutionsRestTestApplication.class, args);
    }

}
