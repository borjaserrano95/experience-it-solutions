package com.experienceitsolutions.resttest.model;

public interface IUser<ID> {
    ID getId();

    String getName();

    String getUsername();

    String getPassword();

    void setName(String name);

    void setUsername(String username);

    void setPassword(String password);

}
